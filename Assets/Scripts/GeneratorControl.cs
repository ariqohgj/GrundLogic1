using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GeneratorControl : MonoBehaviour
{
    public GameObject GeneratorUI;

    public float currentPointR1;
    public float currentPointR2;
    public float currentPointR3;
    public float currentPointR4;
    public float currentPointR5;
    public float _maxPoint;
    public float _speedPoint;
    public Text indikatorPercentage;
    public Slider sld;
    public Text status;

    public bool tutorialDONE = false;

    [Header("Soal Symbol")]
    public GameObject SoalSimbolR1;
    public GameObject SoalSimbolR2;
    public GameObject SoalSimbolR3;
    public GameObject SoalSimbolR4;
    public GameObject SoalSimbolR5;

    [Header("Soal Table")]
    public GameObject SoalTableR1;
    public GameObject SoalTableR2;
    public GameObject SoalTableR3;
    public GameObject SoalTableR4;
    public GameObject SoalTableR5;

    [Header("Soal Fungsi")]
    public GameObject SoalFungsiR1;
    public GameObject SoalFungsiR2;
    public GameObject SoalFungsiR3;
    public GameObject SoalFungsiR4;
    public GameObject SoalFungsiR5;

    [Header("Generator Audio Source")]
    public AudioSource GAudio1;
    public AudioSource GAudio2;
    public AudioSource GAudio3;
    public AudioSource GAudio4;
    public AudioSource GAudio5;

    [Header("Jawaban")]
    public string _jawaban;
    private bool done1_ssimbol = false;
    private bool done1_table = false;
    private bool done1_fungsi = false;

    private bool done2_ssimbol = false;
    private bool done2_table = false;
    private bool done2_fungsi = false;

    private bool done3_ssimbol = false;
    private bool done3_table = false;
    private bool done3_fungsi = false;

    private bool done4_ssimbol = false;
    private bool done4_table = false;
    private bool done4_fungsi = false;

    private bool done5_ssimbol = false;
    private bool done5_table = false;
    private bool done5_fungsi = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(GameManager.Instance.gene1 == true)
        {
            
            sld.value = Mathf.MoveTowards(currentPointR1, _maxPoint, Time.deltaTime * _speedPoint);
            currentPointR1 = sld.value;
            if(done1_ssimbol & done1_table == true && currentPointR1 >= _maxPoint)
            {
                status.text = "Done";
            }
            else
            {
                status.text = "Repairing....";
            }

            indikatorPercentage.text = sld.value.ToString("0") + "%";
            if(currentPointR1>=0 && done1_fungsi == false)
            {
                _speedPoint = 0f;
                SoalFungsiR1.SetActive(true);
                if (_jawaban == "SALAH")
                {
                    StartCoroutine(notifJawabanSalah());
                    StartCoroutine(ResetJawaban());
                }
                else if (_jawaban == "BENAR")
                {
                    _speedPoint = 5f;
                    SoalFungsiR1.SetActive(false);
                    StartCoroutine(notifJawabanBenar());
                    StartCoroutine(ResetJawaban());
                    done1_fungsi = true;
                }

            }
            else if (currentPointR1 >= 50 && done1_ssimbol == false)
            {
                _speedPoint = 0f;
                SoalSimbolR1.SetActive(true);
                if (_jawaban == "SALAH")
                {
                    currentPointR1 -= 20F;
                    _speedPoint = 5f;
                    SoalSimbolR1.SetActive(false);
                    StartCoroutine(notifJawabanSalah());
                    StartCoroutine(ResetJawaban());
                }
                else if (_jawaban == "BENAR")
                {
                    _speedPoint = 5f;
                    SoalSimbolR1.SetActive(false);
                    StartCoroutine(notifJawabanBenar());
                    StartCoroutine(ResetJawaban());
                    done1_ssimbol = true;
                }
            }else if (currentPointR1 >= 80 && done1_table == false)
            {
                _speedPoint = 0f;
                SoalTableR1.SetActive(true);
                if (_jawaban == "BENAR")
                {
                    _speedPoint = 5f;
                    SoalTableR1.SetActive(false);
                    StartCoroutine(notifJawabanBenar());
                    StartCoroutine(ResetJawaban());
                    done1_table = true;
                }
                else if (_jawaban == "SALAH")
                {
                    currentPointR1 -= 20f;
                    _speedPoint = 5f;
                    SoalTableR1.SetActive(false);
                    StartCoroutine(notifJawabanSalah());
                    StartCoroutine(ResetJawaban());
                    
                }
            }else if(currentPointR1 >= _maxPoint)
            {
                GameManager.Instance.gene1 = false;
                status.text = "Done";
                GameManager.Instance.Gene1_DONE = true;
                GAudio1.enabled = true;
            }
            
        }
        if (GameManager.Instance.gene2 == true)
        {
            if (done2_ssimbol & done2_table == true && currentPointR2 >= _maxPoint)
            {
                status.text = "Done";
            }
            else
            {
                status.text = "Repairing....";
            }
            sld.value = Mathf.MoveTowards(currentPointR2, _maxPoint, Time.deltaTime * _speedPoint);
            currentPointR2 = sld.value;

            indikatorPercentage.text = sld.value.ToString("0") + "%";
            if (currentPointR2 >= 0 && done2_fungsi == false)
            {
                _speedPoint = 0f;
                SoalFungsiR2.SetActive(true);
                if (_jawaban == "SALAH")
                {
                    StartCoroutine(notifJawabanSalah());
                    StartCoroutine(ResetJawaban());
                }
                else if (_jawaban == "BENAR")
                {
                    _speedPoint = 5f;
                    SoalFungsiR2.SetActive(false);
                    StartCoroutine(notifJawabanBenar());
                    StartCoroutine(ResetJawaban());
                    done2_fungsi = true;
                }

            }
            else if (currentPointR2 >= 50 && done2_ssimbol == false)
            {
                _speedPoint = 0f;
                SoalSimbolR2.SetActive(true);
                if (_jawaban == "SALAH")
                {
                    currentPointR2 -= 20F;
                    _speedPoint = 5f;
                    SoalSimbolR2.SetActive(false);
                    StartCoroutine(notifJawabanSalah());
                    StartCoroutine(ResetJawaban());
                }
                else if (_jawaban == "BENAR")
                {
                    _speedPoint = 5f;
                    SoalSimbolR2.SetActive(false);
                    StartCoroutine(notifJawabanBenar());
                    StartCoroutine(ResetJawaban());
                    done2_ssimbol = true;
                }
            }
            else if (currentPointR2 >= 80 && done2_table == false)
            {
                _speedPoint = 0f;
                SoalTableR2.SetActive(true);
                if (_jawaban == "BENAR")
                {
                    _speedPoint = 5f;
                    SoalTableR2.SetActive(false);
                    StartCoroutine(notifJawabanBenar());
                    StartCoroutine(ResetJawaban());
                    done2_table = true;
                }
                else if (_jawaban == "SALAH")
                {
                    currentPointR2 -= 20f;
                    _speedPoint = 5f;
                    SoalTableR2.SetActive(false);
                    StartCoroutine(notifJawabanSalah());
                    StartCoroutine(ResetJawaban());
                }
            }
            else if (currentPointR2 >= _maxPoint)
            {
                GameManager.Instance.gene2 = false;
                status.text = "Done";
                GameManager.Instance.Gene2_DONE = true;
                GAudio2.enabled = true;
            }

        }
        if (GameManager.Instance.gene3 == true)
        {
            if (done3_ssimbol & done3_table == true && currentPointR3 >= _maxPoint)
            {
                status.text = "Done";
            }
            else
            {
                status.text = "Repairing....";
            }

            sld.value = Mathf.MoveTowards(currentPointR3, _maxPoint, Time.deltaTime * _speedPoint);
            currentPointR3 = sld.value;

            indikatorPercentage.text = sld.value.ToString("0") + "%";
            if (currentPointR3 >= 0 && done3_fungsi == false)
            {
                _speedPoint = 0f;
                SoalFungsiR3.SetActive(true);
                if (_jawaban == "SALAH")
                {
                    StartCoroutine(notifJawabanSalah());
                    StartCoroutine(ResetJawaban());
                }
                else if (_jawaban == "BENAR")
                {
                    _speedPoint = 5f;
                    SoalFungsiR3.SetActive(false);
                    StartCoroutine(notifJawabanBenar());
                    StartCoroutine(ResetJawaban());
                    done3_fungsi = true;
                }

            }
            else if (currentPointR3 >= 50 && done3_ssimbol == false)
            {
                _speedPoint = 0f;
                SoalSimbolR3.SetActive(true);
                if (_jawaban == "SALAH")
                {
                    currentPointR3 -= 20F;
                    _speedPoint = 5f;
                    SoalSimbolR3.SetActive(false);
                    StartCoroutine(notifJawabanSalah());
                    StartCoroutine(ResetJawaban());
                }
                else if (_jawaban == "BENAR")
                {
                    _speedPoint = 5f;
                    SoalSimbolR3.SetActive(false);
                    StartCoroutine(notifJawabanBenar());
                    StartCoroutine(ResetJawaban());
                    done3_ssimbol = true;
                }
            }
            else if (currentPointR3 >= 80 && done3_table == false)
            {
                _speedPoint = 0f;
                SoalTableR3.SetActive(true);
                if (_jawaban == "BENAR")
                {
                    _speedPoint = 5f;
                    SoalTableR3.SetActive(false);
                    StartCoroutine(notifJawabanBenar());
                    StartCoroutine(ResetJawaban());
                    done3_table = true;
                }
                else if (_jawaban == "SALAH")
                {
                    currentPointR3 -= 20f;
                    _speedPoint = 5f;
                    SoalTableR3.SetActive(false);
                    StartCoroutine(notifJawabanSalah());
                    StartCoroutine(ResetJawaban());
                }
            }
            else if (currentPointR3 >= _maxPoint)
            {
                GameManager.Instance.gene3 = false;
                status.text = "Done";
                GameManager.Instance.Gene3_DONE = true;
                GAudio3.enabled = true;
            }

        }
        if (GameManager.Instance.gene4 == true)
        {
            if (done4_ssimbol & done4_table == true && currentPointR4 >= _maxPoint)
            {
                status.text = "Done";
            }
            else
            {
                status.text = "Repairing....";
            }
            sld.value = Mathf.MoveTowards(currentPointR4, _maxPoint, Time.deltaTime * _speedPoint);
            currentPointR4 = sld.value;

            indikatorPercentage.text = sld.value.ToString("0") + "%";
            if (currentPointR4 >= 0 && done4_fungsi == false)
            {
                _speedPoint = 0f;
                SoalFungsiR4.SetActive(true);
                if (_jawaban == "SALAH")
                {
                    StartCoroutine(notifJawabanSalah());
                    StartCoroutine(ResetJawaban());
                }
                else if (_jawaban == "BENAR")
                {
                    _speedPoint = 5f;
                    SoalFungsiR4.SetActive(false);
                    StartCoroutine(notifJawabanBenar());
                    StartCoroutine(ResetJawaban());
                    done4_fungsi = true;
                }

            }
            else if (currentPointR4 >= 50 && done4_ssimbol == false)
            {
                _speedPoint = 0f;
                SoalSimbolR4.SetActive(true);
                if (_jawaban == "SALAH")
                {
                    currentPointR4 -= 20F;
                    _speedPoint = 5f;
                    SoalSimbolR4.SetActive(false);
                    StartCoroutine(notifJawabanSalah());
                    StartCoroutine(ResetJawaban());
                }
                else if (_jawaban == "BENAR")
                {
                    _speedPoint = 5f;
                    SoalSimbolR4.SetActive(false);
                    StartCoroutine(notifJawabanBenar());
                    StartCoroutine(ResetJawaban());
                    done4_ssimbol = true;
                }
            }
            else if (currentPointR4 >= 80 && done4_table == false)
            {
                _speedPoint = 0f;
                SoalTableR4.SetActive(true);
                if (_jawaban == "BENAR")
                {
                    _speedPoint = 5f;
                    SoalTableR4.SetActive(false);
                    StartCoroutine(notifJawabanBenar());
                    StartCoroutine(ResetJawaban());
                    done4_table = true;
                }
                else if (_jawaban == "SALAH")
                {
                    currentPointR4 -= 20f;
                    _speedPoint = 5f;
                    SoalTableR4.SetActive(false);
                    StartCoroutine(notifJawabanSalah());
                    StartCoroutine(ResetJawaban());
                }
            }
            else if (currentPointR4 >= _maxPoint)
            {
                GameManager.Instance.gene4 = false;
                status.text = "Done";
                GameManager.Instance.Gene4_DONE = true;
                GAudio4.enabled = true;
            }

        }
        if (GameManager.Instance.gene5 == true)
        {
            if (done5_ssimbol & done5_table == true && currentPointR5 >= _maxPoint)
            {
                status.text = "Done";
            }
            else
            {
                status.text = "Repairing....";
            }
            sld.value = Mathf.MoveTowards(currentPointR5, _maxPoint, Time.deltaTime * _speedPoint);
            currentPointR5 = sld.value;

            indikatorPercentage.text = sld.value.ToString("0") + "%";
            if (currentPointR5 >= 0 && done5_fungsi == false)
            {
                _speedPoint = 0f;
                SoalFungsiR5.SetActive(true);
                if (_jawaban == "SALAH")
                {
                    StartCoroutine(notifJawabanSalah());
                    StartCoroutine(ResetJawaban());
                }
                else if (_jawaban == "BENAR")
                {
                    _speedPoint = 5f;
                    SoalFungsiR5.SetActive(false);
                    StartCoroutine(notifJawabanBenar());
                    StartCoroutine(ResetJawaban());
                    done5_fungsi = true;
                }

            }
            else if (currentPointR5 >= 50 && done5_ssimbol == false)
            {
                _speedPoint = 0f;
                SoalSimbolR5.SetActive(true);
                if (_jawaban == "SALAH")
                {
                    currentPointR5 -= 20F;
                    _speedPoint = 5f;
                    SoalSimbolR5.SetActive(false);
                    StartCoroutine(notifJawabanSalah());
                    StartCoroutine(ResetJawaban());
                }
                else if (_jawaban == "BENAR")
                {
                    _speedPoint = 5f;
                    SoalSimbolR5.SetActive(false);
                    StartCoroutine(notifJawabanBenar());
                    StartCoroutine(ResetJawaban());
                    done5_ssimbol = true;
                }
            }
            else if (currentPointR5 >= 80 && done5_table == false)
            {
                _speedPoint = 0f;
                SoalTableR5.SetActive(true);
                if (_jawaban == "BENAR")
                {
                    _speedPoint = 5f;
                    SoalTableR5.SetActive(false);
                    StartCoroutine(notifJawabanBenar());
                    StartCoroutine(ResetJawaban());
                    done5_table = true;
                }
                else if (_jawaban == "SALAH")
                {
                    currentPointR5 -= 20f;
                    _speedPoint = 5f;
                    SoalTableR5.SetActive(false);
                    StartCoroutine(notifJawabanSalah());
                    StartCoroutine(ResetJawaban());
                }
            }
            else if (currentPointR5 >= _maxPoint)
            {
                GameManager.Instance.gene5 = false;
                status.text = "Done";
                GameManager.Instance.Gene5_DONE = true;
                GAudio5.enabled = true;
            }

        }
    }

    public void jawabansoal(string jawaban)
    {
        _jawaban = jawaban;
    }
    IEnumerator ResetJawaban()
    {
        yield return new WaitForSeconds(1f);
        _jawaban = " ";
        
    }
    IEnumerator notifJawabanBenar()
    {

        GameManager.Instance.notifText.text = "Bagus!! Pertahankan";
        GameManager.Instance.notifOBJ.SetActive(true);
        GameManager.Instance.notifPanel.SetActive(true);
        yield return new WaitForSeconds(3);
        GameManager.Instance.notif.SetBool("UP", true);
        GameManager.Instance.notifOBJ.SetActive(false);
        yield return new WaitForSeconds(2);
        GameManager.Instance.notifPanel.SetActive(false);
    }
    IEnumerator notifJawabanSalah()
    {

        GameManager.Instance.notifText.text = "Oh tidak!! Perbaikan -20%";
        GameManager.Instance.notifOBJ.SetActive(true);
        yield return new WaitForSeconds(3);
        GameManager.Instance.notifOBJ.SetActive(false);
    }
    public void tutuo()
    {

    }
    public void tutup()
    {
        if(tutorialDONE == true)
        {
            GameManager.Instance.pause = false;
            GeneratorUI.SetActive(false);
            SoalSimbolR1.SetActive(false);
            SoalSimbolR2.SetActive(false);
            SoalSimbolR3.SetActive(false);
            SoalSimbolR4.SetActive(false);
            SoalSimbolR5.SetActive(false);
            SoalTableR1.SetActive(false);
            SoalTableR2.SetActive(false);
            SoalTableR3.SetActive(false);
            SoalTableR4.SetActive(false);
            SoalTableR5.SetActive(false);

            GameManager.Instance.Key.SetActive(true);
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            GameManager.Instance.charaMove = true;
        }
        else if (tutorialDONE == false)
        {
            GeneratorUI.SetActive(false);
            SoalSimbolR1.SetActive(false);
            SoalSimbolR2.SetActive(false);
            SoalSimbolR3.SetActive(false);
            SoalSimbolR4.SetActive(false);
            SoalSimbolR5.SetActive(false);
            SoalTableR1.SetActive(false);
            SoalTableR2.SetActive(false);
            SoalTableR3.SetActive(false);
            SoalTableR4.SetActive(false);
            SoalTableR5.SetActive(false);
            GameManager.Instance.Key.SetActive(true);
            GameManager.Instance.charaMove = false;
            GameManager.Instance.panelStory.SetActive(true);
            GameManager.Instance.hideUI.SetActive(false);
            GameManager.Instance.panelMisi5.SetActive(true);
            tutorialDONE = true;
        }

    }
}
