using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ID_Obj_Control : MonoBehaviour
{
    public string OBJ_ID;
    // Start is called before the first frame update
    void Start()
    {
        switch (this.OBJ_ID)
        {
            case "SWCH001_1":
                if(GameManager.Instance.SWCH001_1 == false)
                {
                    StartCoroutine(changematOFF());
                    this.gameObject.GetComponent<Animator>().SetBool("OnOff", false);
                }
                else
                {
                    StartCoroutine(changematON());
                    this.gameObject.GetComponent<Animator>().SetBool("OnOff", true);
                }
                break;
            case "SWCH001_2":
                if (GameManager.Instance.SWCH001_2 == false)
                {
                    StartCoroutine(changematOFF());
                    this.gameObject.GetComponent<Animator>().SetBool("OnOff", false);
                }
                else
                {
                    StartCoroutine(changematON());
                    this.gameObject.GetComponent<Animator>().SetBool("OnOff", true);
                }
                break;
            case "SWCH002_1":
                if (GameManager.Instance.SWCH002_1 == false)
                {
                    StartCoroutine(changematOFF());
                    this.gameObject.GetComponent<Animator>().SetBool("OnOff", false);
                }
                else
                {
                    StartCoroutine(changematON());
                    this.gameObject.GetComponent<Animator>().SetBool("OnOff", true);
                }
                break;
            case "SWCH002_2":
                if (GameManager.Instance.SWCH002_2 == false)
                {
                    StartCoroutine(changematOFF());
                    this.gameObject.GetComponent<Animator>().SetBool("OnOff", false);
                }
                else
                {
                    StartCoroutine(changematON());
                    this.gameObject.GetComponent<Animator>().SetBool("OnOff", true);
                }
                break;
            case "SWCH003_1":
                if (GameManager.Instance.SWCH003_1 == false)
                {
                    StartCoroutine(changematOFF());
                    this.gameObject.GetComponent<Animator>().SetBool("OnOff", false);
                }
                else
                {
                    StartCoroutine(changematON());
                    this.gameObject.GetComponent<Animator>().SetBool("OnOff", true);
                }
                break;
            case "SWCH004_1":
                if (GameManager.Instance.SWCH004_1 == false)
                {
                    StartCoroutine(changematOFF());
                    this.gameObject.GetComponent<Animator>().SetBool("OnOff", false);
                }
                else
                {
                    StartCoroutine(changematON());
                    this.gameObject.GetComponent<Animator>().SetBool("OnOff", true);
                }
                break;
            case "SWCH004_2":
                if (GameManager.Instance.SWCH004_2 == false)
                {
                    StartCoroutine(changematOFF());
                    this.gameObject.GetComponent<Animator>().SetBool("OnOff", false);
                }
                else
                {
                    StartCoroutine(changematON());
                    this.gameObject.GetComponent<Animator>().SetBool("OnOff", true);
                }
                break;
            case "SWCH005_1":
                if (GameManager.Instance.SWCH005_1 == false)
                {
                    StartCoroutine(changematOFF());
                    this.gameObject.GetComponent<Animator>().SetBool("OnOff", false);
                }
                else
                {
                    StartCoroutine(changematON());
                    this.gameObject.GetComponent<Animator>().SetBool("OnOff", true);
                }
                break;
            case "SWCH005_2":
                if (GameManager.Instance.SWCH005_2 == false)
                {
                    StartCoroutine(changematOFF());
                    this.gameObject.GetComponent<Animator>().SetBool("OnOff", false);
                }
                else
                {
                    StartCoroutine(changematON());
                    this.gameObject.GetComponent<Animator>().SetBool("OnOff", true);
                }
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator changematON()
    {
        yield return new WaitForSeconds(1.4f);
        this.gameObject.GetComponentInChildren<MeshRenderer>().material = GameManager.Instance.saklarON;
    }
    IEnumerator changematOFF()
    {
        yield return new WaitForSeconds(1.4f);
        this.gameObject.GetComponentInChildren<MeshRenderer>().material = GameManager.Instance.saklarOFF;
    }
}
