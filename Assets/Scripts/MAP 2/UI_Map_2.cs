using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Animations;

public class UI_Map_2 : MonoBehaviour
{
    public TextMeshProUGUI textDisplay;
    public GameObject panelPassword;
    public bool hasOpen=false;
    public Animator anim;
    public Image indikator;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (hasOpen == true)
        {
            anim.SetBool("hasOpen", true);
            indikator.color = Color.green;
        }
    }
    public void closeDialoge()
    {
        GameManager.Instance.dialogTextOBJ.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        GameManager.Instance.walkingSpeed = GameManager.Instance.walkingSpeedDefault;
        GameManager.Instance.runningSpeed = GameManager.Instance.runningSpeedDefault;
        GameManager.Instance.lookSpeed = GameManager.Instance.lookSpeedDefault;
        GameManager.Instance.jumpSpeed = GameManager.Instance.jumpSpeedDefault;
    }
    public void closePassword()
    {
        GameManager.Instance.clueText.SetActive(false);
        GameManager.Instance.panelPassword.SetActive(false);
        GameManager.Instance.indikator.SetActive(false);
        GameManager.Instance.panelSoal.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        GameManager.Instance.walkingSpeed = GameManager.Instance.walkingSpeedDefault;
        GameManager.Instance.runningSpeed = GameManager.Instance.runningSpeedDefault;
        GameManager.Instance.lookSpeed = GameManager.Instance.lookSpeedDefault;
        GameManager.Instance.jumpSpeed = GameManager.Instance.jumpSpeedDefault;
    }

    public void btnPassword(int number)
    {
        textDisplay.text = textDisplay.text + number.ToString();
    }
    public void btnClear()
    {
        textDisplay.text = "";
    }
    public void openDoor()
    {
        if(textDisplay.text == "147")
        {
            hasOpen = true;
        }
        else if(textDisplay.text == "264")
        {
            GameManager.Instance.WinnerPanel.SetActive(true);
        }
    }
}
