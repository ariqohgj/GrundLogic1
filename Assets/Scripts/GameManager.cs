using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;


public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    [Header("Player Setting")]
    public float walkingSpeed = 7.5f;
    public float runningSpeed = 11.5f;
    public float jumpSpeed = 8.0f;
    public float lookSpeed = 2.0f;

    [Header("Player Default Setting")]
    public float walkingSpeedDefault = 7.5f;
    public float runningSpeedDefault = 11.5f;
    public float lookSpeedDefault = 2.0f;
    public float jumpSpeedDefault = 8.0f;

    [Header("Audio")]
    public AudioSource BGM;
    public Image buttonBGM;
    public TextMeshProUGUI textBGM;
    public bool audioBGM = true;
    public Slider sldVolume;

    [Header("Animator")]
    public Animator SwitchAnim;

    [Header("Gameplay")]
    public bool Play;
    public bool Switch;
    public bool pause;
    public GameObject sldr;
    public GameObject teleport;
    public GameObject bulb1;
    public GameObject Alert;

    [Header("Timer")]
    public bool stopwatchActive = false;
    float currentTime;
    public TextMeshProUGUI currentTimeText;
    public int startMinutes;

    [Header("Generator")]
    public bool gene1 = false;
    public bool gene2 = false;
    public bool gene3 = false;
    public bool gene4 = false;
    public bool gene5 = false;

    [Header("Generator Actived")]
    public bool Gene1_DONE = false;
    public bool Gene2_DONE = false;
    public bool Gene3_DONE = false;
    public bool Gene4_DONE = false;
    public bool Gene5_DONE = false;

    [Header("Matrial Holder")]
    public Material saklarON;
    public Material saklarOFF;
    public Material bulb1Mat;
    public Material bulb11MatNormal;
    public Material SphereTower;

    [Header("Room 1")]
    public Light light1_1;
    public Light light1_2;
    public bool SWCH001_1 = false;
    public bool SWCH001_2 = false;

    [Header("Room 2")]
    public Light light2_1;
    public Light light2_2;
    public bool SWCH002_1 = false;
    public bool SWCH002_2 = false ;

    [Header("Room 3")]
    public bool SWCH003_1 = true;
    public Light light3_1;

    [Header("Room 4")]
    public bool SWCH004_1 = true;
    public bool SWCH004_2 = true;
    public Light light4_1;

    [Header("Room 5")]
    public bool SWCH005_1 = true;
    public bool SWCH005_2 = false;
    public Light light5_1;

    [Header("UI")]
    public GameObject Menu;
    public GameObject Key;
    public GameObject WinnerPanel;
    public GameObject LosePanel;

    [Header("Misi")]
    public Toggle Misi1;
    public Toggle Misi2;
    public Toggle Misi3;
    public Toggle Misi4;

    [Header("Score")]
    public int maxScore = 10000;
    public int score = 0;
    public int Misi1Score = 500;
    public int Misi2Score = 500;
    public int Misi3Score = 500;
    public int Misi4Score = 1500;
    public int maxScoreTime = 7000;
    public int testingscore = 0;
    public int scoreTime;
    public float multiplier = 1;
    public int currentScore;
    public TextMeshProUGUI ScoreText;
    public TextMeshProUGUI ScoreTextinGame;
    public TextMeshProUGUI ScoreTextWinner;
    public TextMeshProUGUI ScoreABCWinner;

    [Header("Kunci")]
    public TextMeshProUGUI ANDText;
    public TextMeshProUGUI ORText;
    public TextMeshProUGUI NOTText;
    public TextMeshProUGUI NANDText;
    public TextMeshProUGUI NORText;
    public bool hasKeyAND = false;
    public bool hasKeyOR = false;
    public bool hasKeyNOT = false;
    public bool hasKeyNAND = false;
    public bool hasKeyNOR = false;

    [Header("Simbol")]
    public GameObject AND;
    public GameObject OR;
    public GameObject NOT;
    public GameObject NAND;
    public GameObject NOR;

    [Header("Tower")]
    public GameObject towerLightAND;
    public GameObject towerLightOR;
    public GameObject towerLightNOT;
    public GameObject towerLightNAND;
    public GameObject towerLightNOR;
    public MeshRenderer towerSpehereAND;
    public MeshRenderer towerSpehereOR;
    public MeshRenderer towerSpehereNOT;
    public MeshRenderer towerSpehereNAND;
    public MeshRenderer towerSpeherNOR;
    public bool towerANDON = false;
    public bool towerORON = false;
    public bool towerNOTON = false;
    public bool towerNANDON = false;
    public bool towerNORON = false;


    [Header("Notif")]
    public GameObject notifOBJ;
    public TextMeshProUGUI notifText;
    public GameObject notifPanel;
    public Animator notif;

    [Header("MAP 2")]
    public TextMeshProUGUI dialogText;
    public GameObject dialogTextOBJ;
    public GameObject clueText;
    public GameObject panelPassword;
    public GameObject indikator;
    public GameObject panelSoal;

    [Header("Instruksi")]
    public GameObject panelMisi1;
    public GameObject panelMisi2;
    public GameObject panelMisi3;
    public GameObject panelMisi4;
    public GameObject panelMisi5;
    public GameObject panelMisi6;
    public TextMeshProUGUI deskripsiText;
    public string deskripsiTextChange;
    public string deskripsiTextChangeGene;
    public int deskripsiTower = 0;
    public int deskripsiGene = 0;

    [Header("Story")]
    public GameObject blinkeye;
    public GameObject panelStory;
    public GameObject hideUI;
    public GameObject marker;
    public GameObject marker1;
    public GameObject marker2;
    public GameObject marker3;
    public GameObject markerOR;
    public GameObject markerNOT;
    public GameObject markerNAND;
    public GameObject markerNOR;
    public Animator cameraMov;
    public TextMeshProUGUI[] textNama;

    [Header("Game START")]
    public GameObject gamestartPanel;
    public TextMeshProUGUI gamestartText;
    public string gamestartTextChange;

    [Header("Game CLEAR")]
    public GameObject gameclearPanel;
    public TextMeshProUGUI winText;
    public TextMeshProUGUI WinDeskription;
    public string WinTextChange;
    public string WinDeskripsiChange;

    [Header("CharaMove")]
    public bool charaMove;

    // Start is called before the first frame update
    private void Awake()
    {
        if(Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    void Start()
    {
        currentTime = startMinutes * 60;

    }

    // Update is called once per frame
    void Update()
    {
        textNama[0].text = PlayerPrefs.GetString("Nama");
        textNama[1].text = PlayerPrefs.GetString("Nama");
        textNama[2].text = PlayerPrefs.GetString("Nama");
        textNama[3].text = PlayerPrefs.GetString("Nama");
        textNama[4].text = PlayerPrefs.GetString("Nama");
        textNama[5].text = PlayerPrefs.GetString("Nama");
        textNama[6].text = PlayerPrefs.GetString("Nama");
        textNama[7].text = PlayerPrefs.GetString("Nama");
        textNama[8].text = PlayerPrefs.GetString("Nama");
        textNama[9].text = PlayerPrefs.GetString("Nama");
        textNama[10].text = PlayerPrefs.GetString("Nama");
        //----------------------
        deskripsiTextChange = deskripsiTower + "/5";
        deskripsiTextChangeGene = deskripsiGene + "/5";
        deskripsiText.text = "Menyalakan Tower " + deskripsiTextChange + "\n" + "Perbaiki Generator " + deskripsiTextChangeGene;
        if (charaMove == false)
        {
            walkingSpeed = 0f;
            runningSpeed = 0f;
            jumpSpeed = 0f;
            lookSpeed = 0f;
            stopwatchActive = false;
        }
        else
        {
            walkingSpeed = walkingSpeedDefault;
            runningSpeed = runningSpeedDefault;
            jumpSpeed = jumpSpeedDefault;
            lookSpeed = lookSpeedDefault;
            stopwatchActive = true;
        }
        //bgm
        if (audioBGM == true)
        {
            if(buttonBGM != null)
            {
                buttonBGM.color = Color.green;
                textBGM.text = "ON";
                BGM.UnPause();
            }
            else
            {

            }

        }
        else if (audioBGM == false)
        {
            buttonBGM.color = Color.red;
            textBGM.text = "OFF";
            BGM.Pause();
        }
        else
        {

        }
        // menu
        if (Input.GetKey(KeyCode.Escape) && pause == false)
        {
            // Lock cursor
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            Menu.SetActive(true);
            charaMove = false;
            pause = true;
        }
        //Timer
        if(stopwatchActive == true)
        {
            currentTime = currentTime - Time.deltaTime;
            if(currentTime <= 0)
            {
                stopwatchActive = false;
                LosePanel.SetActive(true);
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                GameManager.Instance.walkingSpeed = 0f;
                GameManager.Instance.runningSpeed = 0f;
                GameManager.Instance.lookSpeed = 0f;
                GameManager.Instance.jumpSpeed = 0f;
                Start();
            }
        }
        if (stopwatchActive == false)
        {
            score += maxScoreTime;
        }
        maxScoreTime = Mathf.RoundToInt(currentTime * multiplier);
        scoreTime = maxScoreTime; 
        TimeSpan time = TimeSpan.FromSeconds(currentTime);
        currentTimeText.text = time.Minutes.ToString() + ":" + time.Seconds.ToString();

        //room1
        if(Gene1_DONE == true)
        {
            if (SWCH001_1 & SWCH001_2 == true)
            {
                light1_1.enabled = true;
                light1_2.enabled = !light1_2.enabled;
                if (AND)
                {
                    AND.SetActive(true);
                }else if (!AND)
                {

                }
            }
            else if (SWCH001_1 & SWCH001_2 == false)
            {
                light1_1.enabled = false;
                light1_2.enabled = false;
            }
            if (GameManager.Instance.Misi3.isOn == false)
            {
                GameManager.Instance.Misi3.isOn = true;
                return;
            }
        }

        //room2
        if(Gene2_DONE == true)
        {
            if (SWCH002_1 == true || SWCH002_2 == true)
            {
                light2_1.enabled = true;
                light2_2.enabled = true;
                if (OR)
                {
                    OR.SetActive(true);
                }
                else if (!OR)
                {

                }
            }
            else if (SWCH002_1 == false || SWCH002_2 == false)
            {
                light2_1.enabled = false;
                light2_2.enabled = false;
            }
        }

        //room3
        if(Gene3_DONE == true)
        {
            if (SWCH003_1 == true)
            {
                light3_1.enabled = false;
   
            }
            else
            {
                light3_1.enabled = true;
                if (NOT)
                {
                    NOT.SetActive(true);
                }
                else if (!NOT)
                {

                }
            }
        }
        
        //room4
        if(Gene4_DONE == true)
        {
            if (SWCH004_1 == true && SWCH004_2 == true)
            {
                light4_1.enabled = false;
              
            }
            else if (SWCH004_1 == false && SWCH004_2 == false)
            {
                light4_1.enabled = !light4_1.enabled;
                if (NAND)
                {
                    NAND.SetActive(true);
                }
                else if (!NAND)
                {

                }
            }
        }

        //room5
        if(Gene5_DONE == true)
        {
            if (SWCH005_1 == true && SWCH005_2 == true)
            {
                light5_1.enabled = false;

            }

            else if (SWCH005_1 == false || SWCH005_2 == false)
            {
                light5_1.enabled = true;
                if (NOR)
                {
                    NOR.SetActive(true);
                }
                else if (!NOR)
                {

                }
            }

        }


        if(towerANDON == true && towerORON == true && towerNOTON == true && towerNANDON == true && towerNORON == true)
        {
            Misi4.isOn = true;
            teleport.SetActive(true);
            stopwatchActive = false;
            ScoreTextWinner.text = "Score :" + score.ToString("0");
        }


        score =+ testingscore;
        //Misi 1
        if(Misi1 != null)
        {
            if (Misi1.isOn == true)
            {
                score += Misi1Score;
            }
            if (Misi2.isOn == true)
            {
                score += Misi2Score;
            }
            if (Misi3.isOn == true)
            {
                score += Misi3Score;
            }
            if (Misi4.isOn == true)
            {
                score += Misi4Score;
            }
        }
        else
        {

        }
       
        if(ScoreText != null)
        {
            ScoreText.text = score.ToString();
            ScoreTextinGame.text = score.ToString();
        }



        if(currentScore <= 10000)
        {
            if(currentScore > 8000)
            {
                ScoreABCWinner.text = "A";
            }
        }

        if (currentScore <= 8000)
        {
            if (currentScore > 6000)
            {
                ScoreABCWinner.text = "B";
            }
        }
        if (currentScore <= 6000)
        {
            if (currentScore > 3000)
            {
                ScoreABCWinner.text = "C";
            }
        }
        if (currentScore <= 3000)
        {
            if (currentScore > 0)
            {
                ScoreABCWinner.text = "D";
            }
        }

        BGM.volume = sldVolume.value;

    }

    public void SwitchOn()
    {
        SwitchAnim.SetBool("OnOff", true);
    }
    public void SwitchOff()
    {
        SwitchAnim.SetBool("OnOff", false);
    }
    public void kembali()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        Menu.SetActive(false);
        pause = false;
        // Lock cursor
        charaMove = true;
 
    }

    public void ChangeScene(string sceneName)
    {
        SceneManager.LoadSceneAsync(sceneName);
    }

    public void openAlert()
    {
        Alert.SetActive(true);
    }
    public void closeAlert()
    {
        Alert.SetActive(false);
    }

    public void OnOFBGM()
    {
        if (audioBGM)
        {
            audioBGM = false;
        }else if (!audioBGM)
        {
            audioBGM = true;
        }

    }
    public void blinkeyetobutton()
    {
        StartCoroutine(blinkeyeproses());
    }

    IEnumerator blinkeyeproses()
    {
        blinkeye.SetActive(true);
        yield return new WaitForSeconds(3.2f);
        blinkeye.SetActive(false);
        yield return new WaitForSeconds(1f);
        GameManager.Instance.panelStory.SetActive(true);
    }
}
