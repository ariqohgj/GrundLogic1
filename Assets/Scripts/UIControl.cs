using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIControl : MonoBehaviour
{
    public Color redcolor;
    public Color greencolor;

    [Header("Game Setting")]
    public TMP_Dropdown qualityDropdown;

    [Header("Room 1 UI")]
    public Image Saklar1_1;
    public Image Saklar1_2;
    public Image Generator1_1;
    public Image Lampu1;
    [Header("Room 2 UI")]
    public Image Saklar2_1;
    public Image Saklar2_2;
    public Image Generator2_1;
    public Image Lampu2;
    [Header("Room 3 UI")]
    public Image Saklar3_1;
    public Image Generator3_1;
    public Image Relay3;
    public Image Lampu3;
    [Header("Room 4 UI")]
    public Image Saklar4_1;
    public Image Saklar4_2;
    public Image Generator4_1;
    public Image Relay4;
    public Image Lampu4;
    [Header("Room 5 UI")]
    public Image Saklar5_1;
    public Image Saklar5_2;
    public Image Generator5_1;
    public Image Relay5;
    public Image Lampu5;

    [Header("Side Tab")]
    public GameObject Room1;
    public GameObject Room2;
    public GameObject Room3;
    public GameObject Room4;
    public GameObject Room5;
    [Header("Upper Tab")]
    public GameObject Objektif;
    public GameObject Blueprint;
    public GameObject Setting;
    [Header("Sidebar")]
    public GameObject sideabr1;

    // Start is called before the first frame update
    void Start()
    {
        redcolor = Color.red;
        greencolor = Color.green;
        qualityDropdown.value = QualitySettings.GetQualityLevel();
        
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Room1.activeInHierarchy)
        {
            if (GameManager.Instance.SWCH001_1 == true)
            {
                Saklar1_1.color = greencolor;
            }
            else
            {
                Saklar1_1.color = redcolor;
            }
            //---------------------------------------------------//
            if (GameManager.Instance.SWCH001_2 == true)
            {
                Saklar1_2.color = greencolor;
            }
            else
            {
                Saklar1_2.color = redcolor;
            }
            //---------------------------------------------------//
            if (GameManager.Instance.Gene1_DONE == true)
            {
                Generator1_1.color = greencolor;
            }
            else
            {
                Generator1_1.color = redcolor;
            }
            //---------------------------------------------------//
            if (GameManager.Instance.SWCH001_2 == true && GameManager.Instance.SWCH001_1 == true)
            {
                Lampu1.color = greencolor;
            }
            else
            {
                Lampu1.color = redcolor;
            }
        }
        if (Room2.activeInHierarchy)
        {
            //---------------------------------------------------//
            if (GameManager.Instance.SWCH002_1 == true)
            {
                Saklar2_1.color = greencolor;
            }
            else
            {
                Saklar2_1.color = redcolor;
            }
            //---------------------------------------------------//
            if (GameManager.Instance.SWCH002_2 == true)
            {
                Saklar2_2.color = greencolor;
            }
            else
            {
                Saklar2_2.color = redcolor;
            }
            //---------------------------------------------------//
            if (GameManager.Instance.Gene2_DONE == true)
            {
                Generator2_1.color = greencolor;
            }
            else
            {
                Generator2_1.color = redcolor;
            }
            //---------------------------------------------------//
            if (GameManager.Instance.SWCH002_2 == true || GameManager.Instance.SWCH002_1 == true)
            {
                Lampu2.color = greencolor;
            }
            else
            {
                Lampu2.color = redcolor;
            }
        }
        if (Room3.activeInHierarchy)
        {
            //---------------------------------------------------//
            if (GameManager.Instance.SWCH003_1 == true)
            {
                Saklar3_1.color = greencolor;
            }
            else
            {
                Saklar3_1.color = redcolor;
            }
            //---------------------------------------------------//
            if (GameManager.Instance.SWCH003_1 == true)
            {
                Relay3.color = greencolor;
            }
            else
            {
                Relay3.color = redcolor;
            }
            if (GameManager.Instance.Gene3_DONE == true)
            {
                Generator3_1.color = greencolor;
            }
            else
            {
                Generator3_1.color = redcolor;
            }
            //---------------------------------------------------//
            if (GameManager.Instance.SWCH003_1 == true )
            {
                Lampu3.color = redcolor;
            }
            else
            {
                Lampu3.color = greencolor;
            }
        }
        if (Room4.activeInHierarchy)
        {
            //---------------------------------------------------//
            if (GameManager.Instance.SWCH004_1 == true)
            {
                Saklar4_1.color = greencolor;
            }
            else
            {
                Saklar4_1.color = redcolor;
            }
            //---------------------------------------------------//
            if (GameManager.Instance.SWCH004_2 == true)
            {
                Saklar4_2.color = greencolor;
            }
            else
            {
                Saklar4_2.color = redcolor;
            }
            //---------------------------------------------------//
            if(GameManager.Instance.SWCH004_1 == true  && GameManager.Instance.SWCH004_2 == true)
            {

                Relay4.color = greencolor;
            }
            else
            {
                Relay4.color = redcolor;
            }
            if (GameManager.Instance.Gene4_DONE == true)
            {
                Generator4_1.color = greencolor;
            }
            else
            {
                Generator4_1.color = redcolor;
            }
            //---------------------------------------------------//
            if (GameManager.Instance.SWCH004_2 == true && GameManager.Instance.SWCH004_1 == true)
            {
                Lampu4.color = redcolor;
            }
            else
            {
                Lampu4.color = greencolor;
            }
        }
        if (Room5.activeInHierarchy)
        {

            //---------------------------------------------------//
            if (GameManager.Instance.SWCH005_1 == true)
            {
                Saklar5_1.color = greencolor;
            }
            else
            {
                Saklar5_1.color = redcolor;
            }
            //---------------------------------------------------//
            if (GameManager.Instance.SWCH005_2 == true)
            {
                Saklar5_2.color = greencolor;
            }
            else
            {
                Saklar5_2.color = redcolor;
            }
            //---------------------------------------------------//
            if(GameManager.Instance.SWCH005_1 == true && GameManager.Instance.SWCH005_2 == true)
            {
                
                Relay5.color = greencolor;
            }
            else if(GameManager.Instance.SWCH005_1 == false || GameManager.Instance.SWCH005_2 == false)
            {
                Relay5.color = redcolor;
            }
            if (GameManager.Instance.Gene5_DONE == true)
            {
                Generator5_1.color = greencolor;
            }
            else
            {
                Generator5_1.color = redcolor;
            }
            if (GameManager.Instance.SWCH005_1 == true && GameManager.Instance.SWCH005_2 == true)
            {

                Lampu5.color = redcolor;
            }
            else if (GameManager.Instance.SWCH005_1 == false || GameManager.Instance.SWCH005_2 == false)
            {
                Lampu5.color = greencolor;
            }
        }
    }

    public void clickchangesidetab(int number)
    {
        switch (number)
        {
            case 1:
                Room1.SetActive(true);
                Room2.SetActive(false);
                Room3.SetActive(false);
                Room4.SetActive(false);
                Room5.SetActive(false);
                break;
            case 2:
                Room1.SetActive(false);
                Room2.SetActive(true);
                Room3.SetActive(false);
                Room4.SetActive(false);
                Room5.SetActive(false);
                break;
            case 3:
                Room1.SetActive(false);
                Room2.SetActive(false);
                Room3.SetActive(true);
                Room4.SetActive(false);
                Room5.SetActive(false);
                break;
            case 4:
                Room1.SetActive(false);
                Room2.SetActive(false);
                Room3.SetActive(false);
                Room4.SetActive(true);
                Room5.SetActive(false);
                break;
            case 5:
                Room1.SetActive(false);
                Room2.SetActive(false);
                Room3.SetActive(false);
                Room4.SetActive(false);
                Room5.SetActive(true);
                break;

        }
    }
    public void cliclchangeuptab(int number)
    {
        switch (number)
        {
            case 1:
                Objektif.SetActive(true);
                Blueprint.SetActive(false);
                Setting.SetActive(false);
                sideabr1.SetActive(false);
                break;
            case 2:
                Objektif.SetActive(false);
                Blueprint.SetActive(true);
                Setting.SetActive(false);
                sideabr1.SetActive(true);
                break;
            case 3:
                Objektif.SetActive(false);
                Blueprint.SetActive(false);
                Setting.SetActive(true);
                sideabr1.SetActive(false);
                break;
        }
    }
    public void SetQuality(int qualityIndex)
    {
        if (qualityIndex != 3) // if the user is not using 
                               //any of the presets
            QualitySettings.SetQualityLevel(qualityIndex);
        switch (qualityIndex)
        {
            case 0: // quality level - low

                break;
            case 1: // quality level - medium

                break;
            case 2: // quality level - high

                break;

        }
        qualityDropdown.value = qualityIndex;
    }
    public void SaveSettings()
    {
        PlayerPrefs.SetInt("QualitySettingPreference",
                   qualityDropdown.value);
    }
}
