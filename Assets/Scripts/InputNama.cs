using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class InputNama : MonoBehaviour
{
    public TMP_InputField inputnama_;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void input()
    {
        PlayerPrefs.SetString("Nama", inputnama_.text);
        SceneManager.LoadScene("Opening");
    }
}
