﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[RequireComponent(typeof(CharacterController))]

public class SC_FPSController : MonoBehaviour
{
    [Header("Player Setting")]
    public float walkingSpeed = 0;
    public float runningSpeed = 0;
    public float jumpSpeed = 0;
    public float gravity = 20.0f;
    public Camera playerCamera;
    public float lookSpeed = 0;
    public float lookXLimit = 45.0f;

    CharacterController characterController;
    Vector3 moveDirection = Vector3.zero;
    float rotationX = 0;
    public AudioSource audioWalk;

    [HideInInspector]
    public bool canMove = true;
    public bool isMoving;

    void Start()
    {
        characterController = GetComponent<CharacterController>();

        // Lock cursor
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

    }

    void Update()
    {
        //Set Player Setting through game manager
        walkingSpeed = GameManager.Instance.walkingSpeed;
        runningSpeed = GameManager.Instance.runningSpeed;
        jumpSpeed = GameManager.Instance.jumpSpeed;
        lookSpeed = GameManager.Instance.lookSpeed;
        // We are grounded, so recalculate move direction based on axes
        Vector3 forward = transform.TransformDirection(Vector3.forward);
        Vector3 right = transform.TransformDirection(Vector3.right);
        // Press Left Shift to run
        bool isRunning = Input.GetKey(KeyCode.LeftShift);
        float curSpeedX = canMove ? (isRunning ? runningSpeed : walkingSpeed) * Input.GetAxis("Vertical") : 0;
        float curSpeedY = canMove ? (isRunning ? runningSpeed : walkingSpeed) * Input.GetAxis("Horizontal") : 0;
        float movementDirectionY = moveDirection.y;
        moveDirection = (forward * curSpeedX) + (right * curSpeedY);

        
        if(Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0)
        {
            isMoving = true;
        }
        else
        {
            isMoving = false;
        }
        if (isMoving)
        {
            if (!audioWalk.isPlaying)
            {
                audioWalk.Play();
            }

        }
        else
        {
            audioWalk.Stop();
        }

        if (isRunning)
        {
            audioWalk.pitch = 2;
        }
        else
        {
            audioWalk.pitch = 1;
        }

        if (Input.GetButton("Jump") && canMove && characterController.isGrounded)
        {
            moveDirection.y = jumpSpeed;
        }
        else
        {
            moveDirection.y = movementDirectionY;
        }

        // Apply gravity. Gravity is multiplied by deltaTime twice (once here, and once below
        // when the moveDirection is multiplied by deltaTime). This is because gravity should be applied
        // as an acceleration (ms^-2)
        if (!characterController.isGrounded)
        {
            moveDirection.y -= gravity * Time.deltaTime;
        }

        // Move the controller
        characterController.Move(moveDirection * Time.deltaTime);

        // Player and Camera rotation
        if (canMove)
        {
            rotationX += -Input.GetAxis("Mouse Y") * lookSpeed;
            rotationX = Mathf.Clamp(rotationX, -lookXLimit, lookXLimit);
            playerCamera.transform.localRotation = Quaternion.Euler(rotationX, 0, 0);
            transform.rotation *= Quaternion.Euler(0, Input.GetAxis("Mouse X") * lookSpeed, 0);
        }

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Finish"))
        {
            StartCoroutine(winner());
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            GameManager.Instance.stopwatchActive = false;
            GameManager.Instance.currentScore = GameManager.Instance.score + GameManager.Instance.scoreTime;
            GameManager.Instance.walkingSpeed = 0f;
            GameManager.Instance.runningSpeed = 0f;
            GameManager.Instance.lookSpeed = 0f;
            GameManager.Instance.jumpSpeed = 0f;
            
        }
        if (other.CompareTag("Marker"))
        {
            switch (other.gameObject.GetComponent<ID_Obj_Control>().OBJ_ID)
            {
                case "M001":
                    GameManager.Instance.panelStory.SetActive(true);
                    GameManager.Instance.charaMove = false;
                    GameManager.Instance.hideUI.SetActive(false);
                    GameManager.Instance.panelMisi1.SetActive(false);
                    GameManager.Instance.panelMisi2.SetActive(true);
                    GameManager.Instance.pause = true;
                    Cursor.lockState = CursorLockMode.None;
                    Cursor.visible = true;
                    StartCoroutine(hidemarker());
                    break;
                case "M002":
                    GameManager.Instance.marker1.SetActive(false);
                    GameManager.Instance.charaMove = false;
                    GameManager.Instance.hideUI.SetActive(false);
                    GameManager.Instance.pause = true;
                    GameManager.Instance.cameraMov.enabled = true;
                    GameManager.Instance.panelMisi2.SetActive(false);
                    GameManager.Instance.panelMisi3.SetActive(true);
                    StartCoroutine(headAnim());
                    break;
                case "M003":
                    GameManager.Instance.panelMisi3.SetActive(false);
                    GameManager.Instance.panelMisi4.SetActive(true);
                    GameManager.Instance.marker2.SetActive(false);
                    GameManager.Instance.pause = true;
                    break;
                case "M004":

                    GameManager.Instance.marker3.SetActive(false);
                    GameManager.Instance.pause = true;
                    StartCoroutine(closedialogterakhir());
                    break;
                case "MOR":
                    GameManager.Instance.markerOR.SetActive(false);
                    GameManager.Instance.gamestartTextChange = "GERBANG OR START!!!";
                    GameManager.Instance.gamestartText.text = GameManager.Instance.gamestartTextChange;
                    GameManager.Instance.gamestartPanel.SetActive(true);
                    GameManager.Instance.pause = true;
                    Cursor.lockState = CursorLockMode.None;
                    Cursor.visible = true;
                    GameManager.Instance.charaMove = false;
                    break;
                case "MNOT":
                    GameManager.Instance.markerNOT.SetActive(false);
                    GameManager.Instance.gamestartTextChange = "GERBANG NOT START!!!";
                    GameManager.Instance.gamestartText.text = GameManager.Instance.gamestartTextChange;
                    GameManager.Instance.gamestartPanel.SetActive(true);
                    GameManager.Instance.pause = true;
                    Cursor.lockState = CursorLockMode.None;
                    Cursor.visible = true;
                    GameManager.Instance.charaMove = false;
                    break;
                case "MNAND":
                    GameManager.Instance.markerNAND.SetActive(false);
                    GameManager.Instance.gamestartTextChange = "GERBANG NAND START!!!";
                    GameManager.Instance.gamestartText.text = GameManager.Instance.gamestartTextChange;
                    GameManager.Instance.gamestartPanel.SetActive(true);
                    GameManager.Instance.pause = true;
                    Cursor.lockState = CursorLockMode.None;
                    Cursor.visible = true;
                    GameManager.Instance.charaMove = false;
                    break;
                case "MNOR":
                    GameManager.Instance.markerNOR.SetActive(false);
                    GameManager.Instance.gamestartTextChange = "GERBANG NOR START!!!";
                    GameManager.Instance.gamestartText.text = GameManager.Instance.gamestartTextChange;
                    GameManager.Instance.gamestartPanel.SetActive(true);
                    GameManager.Instance.pause = true;
                    Cursor.lockState = CursorLockMode.None;
                    Cursor.visible = true;
                    GameManager.Instance.charaMove = false;
                    break;
            }
            
        }
    }

    IEnumerator winner()
    {
        yield return new WaitForSeconds(3);
        GameManager.Instance.WinnerPanel.SetActive(true);
    }
     IEnumerator hidemarker()
    {
        yield return new WaitForSeconds(0.5f);
        GameManager.Instance.marker.SetActive(false);
        yield return new WaitForSeconds(2f);
        GameManager.Instance.marker1.SetActive(true);
    }

    IEnumerator headAnim()
    {
        yield return new WaitForSeconds(1f);
        GameManager.Instance.cameraMov.SetBool("MoveHead", true);
        yield return new WaitForSeconds(7f);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        GameManager.Instance.panelStory.SetActive(true);
        GameManager.Instance.cameraMov.enabled = false;
        yield return new WaitForSeconds(2f);
        GameManager.Instance.marker2.SetActive(true);
        
    }
    IEnumerator closedialogterakhir()
    {
        GameManager.Instance.panelStory.SetActive(true);
        GameManager.Instance.panelMisi6.SetActive(true);
        GameManager.Instance.markerOR.SetActive(true);
        GameManager.Instance.markerNOR.SetActive(true);
        GameManager.Instance.markerNOT.SetActive(true);
        GameManager.Instance.markerNAND.SetActive(true);
        yield return new WaitForSeconds(5);
        GameManager.Instance.panelStory.SetActive(false);
        GameManager.Instance.panelMisi4.SetActive(false);
        yield return new WaitForSeconds(2);
        GameManager.Instance.panelMisi5.SetActive(false);
        GameManager.Instance.pause = false;
    }

}