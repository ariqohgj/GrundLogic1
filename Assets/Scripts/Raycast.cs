using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Raycast : MonoBehaviour
{
    private GameObject raycastObj;

    [SerializeField] private int rayLenght = 5;
    [SerializeField] private LayerMask LayermaskInteract;
    [SerializeField] private Image uiCrosshair;
    [SerializeField] private MeshRenderer saklar;
    [SerializeField] private ID_Obj_Control ID_OBJ;
    [SerializeField] private GeneratorControl geneobj;
    [SerializeField] private Animator switchAnim;
    [SerializeField] private GameObject button;
    [SerializeField] private Button buttonINT;
    [SerializeField] private TextContainer textCont;

    // Start is called before the first frame update
    void Start()
    {
        buttonINT = button.GetComponent<Button>();
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        Vector3 fwd = transform.TransformDirection(Vector3.forward);

        if(Physics.Raycast(transform.position,fwd,out hit, rayLenght, LayermaskInteract.value))
        {

            button.SetActive(true);
            ID_OBJ = hit.transform.gameObject.GetComponent<ID_Obj_Control>();
            Debug.Log(ID_OBJ.OBJ_ID);
            if (hit.collider.CompareTag("Brangkas"))
            {
                CrosshairActive();
                if (Input.GetKeyDown(KeyCode.E))
                {
                    GameManager.Instance.panelPassword.SetActive(true);
                    GameManager.Instance.panelSoal.SetActive(true);
                    Cursor.lockState = CursorLockMode.None;
                    Cursor.visible = true;
                    GameManager.Instance.walkingSpeed = 0f;
                    GameManager.Instance.runningSpeed = 0f;
                    GameManager.Instance.lookSpeed = 0f;
                    GameManager.Instance.jumpSpeed = 0f;
                }
            }
            if (hit.collider.CompareTag("Password"))
            {
                CrosshairActive();
                if (Input.GetKeyDown(KeyCode.E))
                {

                    GameManager.Instance.clueText.SetActive(true);
                    GameManager.Instance.panelPassword.SetActive(true);
                    GameManager.Instance.indikator.SetActive(true);
                    Cursor.lockState = CursorLockMode.None;
                    Cursor.visible = true;
                    GameManager.Instance.walkingSpeed = 0f;
                    GameManager.Instance.runningSpeed = 0f;
                    GameManager.Instance.lookSpeed = 0f;
                    GameManager.Instance.jumpSpeed = 0f;
                }
            }
            if (hit.collider.CompareTag("Clue"))
            {
                CrosshairActive();
                textCont = hit.transform.gameObject.GetComponent<TextContainer>();
                GameManager.Instance.dialogText.text = textCont.Text;
                if (Input.GetKeyDown(KeyCode.E))
                {
                    GameManager.Instance.dialogTextOBJ.SetActive(true);
                    Cursor.lockState = CursorLockMode.None;
                    Cursor.visible = true;
                    GameManager.Instance.walkingSpeed = 0f;
                    GameManager.Instance.runningSpeed = 0f;
                    GameManager.Instance.lookSpeed = 0f;
                    GameManager.Instance.jumpSpeed = 0f;
                }
            }
            if (hit.collider.CompareTag("Tower"))
            {
                CrosshairActive();
                if (Input.GetKeyDown(KeyCode.E))
                {
                    buttonINT.onClick.Invoke();
                }
                else
                {

                }
            }
            if (hit.collider.CompareTag("Simbol"))
            {
                CrosshairActive();
                if (Input.GetKeyDown(KeyCode.E))
                {
                    buttonINT.onClick.Invoke();
                }
                else
                {

                }
            }
            else if (hit.collider.CompareTag("Switch"))
            {
                CrosshairActive();
                saklar = hit.transform.gameObject.GetComponentInChildren<MeshRenderer>();
                switchAnim = hit.transform.gameObject.GetComponentInChildren<Animator>();

                if (Input.GetKeyDown(KeyCode.E))
                {
                    buttonINT.onClick.Invoke();
                    
                    if(GameManager.Instance.Misi1.isOn == false)
                    {
                        GameManager.Instance.Misi1.isOn = true;
                    }
                }
            }

            else if (hit.collider.CompareTag("Generator"))
            {
                CrosshairActive();
                if (Input.GetKeyDown(KeyCode.E))
                {
                    GameManager.Instance.pause = true;
                    Cursor.lockState = CursorLockMode.None;
                    Cursor.visible = true;
                    GameManager.Instance.charaMove = false;
                    GameManager.Instance.Key.SetActive(false);
                    buttonINT.onClick.Invoke();
                    if (GameManager.Instance.Misi2.isOn == false)
                    {
                        GameManager.Instance.Misi2.isOn = true;
                    }
                }
                else if (Input.GetKeyDown(KeyCode.F))
                {
                    Cursor.lockState = CursorLockMode.Locked;
                    Cursor.visible = false;
                    GameManager.Instance.Key.SetActive(true);
                    GameManager.Instance.sldr.SetActive(false);
                    GameManager.Instance.walkingSpeed = GameManager.Instance.walkingSpeedDefault;
                    GameManager.Instance.runningSpeed = GameManager.Instance.runningSpeedDefault;
                    GameManager.Instance.lookSpeed = GameManager.Instance.lookSpeedDefault;
                    GameManager.Instance.jumpSpeed = GameManager.Instance.jumpSpeedDefault;
                }

            }
        }
        else
        {
            CrosshairNormal();
            button.SetActive(false);
        }
    }
    void CrosshairActive()
    {
        uiCrosshair.color = Color.green;
    }
    void CrosshairNormal()
    {
        uiCrosshair.color = Color.white;
    }
    IEnumerator changematON()
    {
        yield return new WaitForSeconds(1.4f);
        saklar.material = GameManager.Instance.saklarON;
    }
    IEnumerator changematOFF()
    {
        yield return new WaitForSeconds(1.4f);
        saklar.material = GameManager.Instance.saklarOFF;
    }
    IEnumerator StopAWhile()
    {
        GameManager.Instance.walkingSpeed = 0f;
        GameManager.Instance.runningSpeed = 0f;
        GameManager.Instance.lookSpeed = 0f;
        GameManager.Instance.jumpSpeed = 0f;
        yield return new WaitForSeconds(1f);
        GameManager.Instance.walkingSpeed = GameManager.Instance.walkingSpeedDefault;
        GameManager.Instance.runningSpeed = GameManager.Instance.runningSpeedDefault;
        GameManager.Instance.lookSpeed = GameManager.Instance.lookSpeedDefault;
        GameManager.Instance.jumpSpeed = GameManager.Instance.jumpSpeedDefault;
    }

    public void interaksi()
    {
        switch (ID_OBJ.OBJ_ID)
        {
            //Switch Cotntroller----------------------------------------------------
                //Switch Room 1
            case "SWCH001_1":
                if (switchAnim.GetBool("OnOff") == false)
                {
                    StartCoroutine(StopAWhile());
                    switchAnim.SetBool("OnOff", true);
                    StartCoroutine(changematON());
                    GameManager.Instance.SWCH001_1 = true;
                }else if (switchAnim.GetBool("OnOff") == true)
                {
                    StartCoroutine(StopAWhile());
                    switchAnim.SetBool("OnOff", false);
                    StartCoroutine(changematOFF());
                    GameManager.Instance.SWCH001_1 = false;
                }
                break;
            case "SWCH001_2":
                if (switchAnim.GetBool("OnOff") == false)
                {
                    StartCoroutine(StopAWhile());
                    switchAnim.SetBool("OnOff", true);
                    StartCoroutine(changematON());
                    GameManager.Instance.SWCH001_2 = true;
                }
                else if (switchAnim.GetBool("OnOff") == true)
                {
                    StartCoroutine(StopAWhile());
                    switchAnim.SetBool("OnOff", false);
                    StartCoroutine(changematOFF());
                    GameManager.Instance.SWCH001_2 = false;
                }
                break;


            //Switch Room 2
            case "SWCH002_1":
                if (switchAnim.GetBool("OnOff") == false)
                {
                    StartCoroutine(StopAWhile());
                    switchAnim.SetBool("OnOff", true);
                    StartCoroutine(changematON());
                    GameManager.Instance.SWCH002_1 = true;
                }
                else if (switchAnim.GetBool("OnOff") == true)
                {
                    StartCoroutine(StopAWhile());
                    switchAnim.SetBool("OnOff", false);
                    StartCoroutine(changematOFF());
                    GameManager.Instance.SWCH002_1 = false;
                }
                break;
            case "SWCH002_2":
                if (switchAnim.GetBool("OnOff") == false)
                {
                    StartCoroutine(StopAWhile());
                    switchAnim.SetBool("OnOff", true);
                    StartCoroutine(changematON());
                    GameManager.Instance.SWCH002_2 = true;
                }
                else if (switchAnim.GetBool("OnOff") == true)
                {
                    StartCoroutine(StopAWhile());
                    switchAnim.SetBool("OnOff", false);
                    StartCoroutine(changematOFF());
                    GameManager.Instance.SWCH002_2 = false;
                }
                break;
            //room3
            case "SWCH003_1":
                if (switchAnim.GetBool("OnOff") == false)
                {
                    StartCoroutine(StopAWhile());
                    switchAnim.SetBool("OnOff", true);
                    StartCoroutine(changematON());
                    GameManager.Instance.SWCH003_1 = true;
                }
                else if (switchAnim.GetBool("OnOff") == true)
                {
                    StartCoroutine(StopAWhile());
                    switchAnim.SetBool("OnOff", false);
                    StartCoroutine(changematOFF());
                    GameManager.Instance.SWCH003_1 = false;
                }
                break;
            //room4
            case "SWCH004_1":
                if (switchAnim.GetBool("OnOff") == false)
                {
                    StartCoroutine(StopAWhile());
                    switchAnim.SetBool("OnOff", true);
                    StartCoroutine(changematON());
                    GameManager.Instance.SWCH004_1 = true;
                }
                else if (switchAnim.GetBool("OnOff") == true)
                {
                    StartCoroutine(StopAWhile());
                    switchAnim.SetBool("OnOff", false);
                    StartCoroutine(changematOFF());
                    GameManager.Instance.SWCH004_1 = false;
                }
                break;
            case "SWCH004_2":
                if (switchAnim.GetBool("OnOff") == false)
                {
                    StartCoroutine(StopAWhile());
                    switchAnim.SetBool("OnOff", true);
                    StartCoroutine(changematON());
                    GameManager.Instance.SWCH004_2 = true;
                }
                else if (switchAnim.GetBool("OnOff") == true)
                {
                    StartCoroutine(StopAWhile());
                    switchAnim.SetBool("OnOff", false);
                    StartCoroutine(changematOFF());
                    GameManager.Instance.SWCH004_2 = false;
                }
                break;
            case "SWCH005_1":
                if (switchAnim.GetBool("OnOff") == false)
                {
                    StartCoroutine(StopAWhile());
                    switchAnim.SetBool("OnOff", true);
                    StartCoroutine(changematON());
                    GameManager.Instance.SWCH005_1 = true;
                }
                else if (switchAnim.GetBool("OnOff") == true)
                {
                    StartCoroutine(StopAWhile());
                    switchAnim.SetBool("OnOff", false);
                    StartCoroutine(changematOFF());
                    GameManager.Instance.SWCH005_1 = false;
                }
                break;
            case "SWCH005_2":
                if (switchAnim.GetBool("OnOff") == false)
                {
                    StartCoroutine(StopAWhile());
                    switchAnim.SetBool("OnOff", true);
                    StartCoroutine(changematON());
                    GameManager.Instance.SWCH005_2 = true;
                }
                else if (switchAnim.GetBool("OnOff") == true)
                {
                    StartCoroutine(StopAWhile());
                    switchAnim.SetBool("OnOff", false);
                    StartCoroutine(changematOFF());
                    GameManager.Instance.SWCH005_2 = false;
                }
                break;
            //Generator
            case "GENE001":

                GameManager.Instance.sldr.SetActive(true);
                GameManager.Instance.gene1 = true;
                GameManager.Instance.walkingSpeed = 0f;
                GameManager.Instance.runningSpeed = 0f;
                GameManager.Instance.lookSpeed = 0f;
                GameManager.Instance.jumpSpeed = 0f;
                break;
            case "GENE002":

                GameManager.Instance.sldr.SetActive(true);
                GameManager.Instance.gene2 = true;
                GameManager.Instance.walkingSpeed = 0f;
                GameManager.Instance.runningSpeed = 0f;
                GameManager.Instance.lookSpeed = 0f;
                GameManager.Instance.jumpSpeed = 0f;
                break;
            case "GENE003":

                GameManager.Instance.sldr.SetActive(true);
                GameManager.Instance.gene3 = true;
                GameManager.Instance.walkingSpeed = 0f;
                GameManager.Instance.runningSpeed = 0f;
                GameManager.Instance.lookSpeed = 0f;
                GameManager.Instance.jumpSpeed = 0f;
                break;
            case "GENE004":

                GameManager.Instance.sldr.SetActive(true);
                GameManager.Instance.gene4 = true;
                GameManager.Instance.walkingSpeed = 0f;
                GameManager.Instance.runningSpeed = 0f;
                GameManager.Instance.lookSpeed = 0f;
                GameManager.Instance.jumpSpeed = 0f;
                break;
            case "GENE005":

                GameManager.Instance.sldr.SetActive(true);
                GameManager.Instance.gene5 = true;
                GameManager.Instance.walkingSpeed = 0f;
                GameManager.Instance.runningSpeed = 0f;
                GameManager.Instance.lookSpeed = 0f;
                GameManager.Instance.jumpSpeed = 0f;
                break;


            //simbol
            case "AND":
                GameManager.Instance.WinTextChange = "GERBANG AND SELESAI";
                GameManager.Instance.WinDeskripsiChange = "SELAMAT!!  kamu telah menyelesaikan perbaikan salah satu ruangan dengan menggunakan gerbang logika AND. Kamu telah memperbaiki generator dan menyalakan kedua saklar yang merupakan cara mengalirkan arus menggunakan gerbang logika AND.";
                StartCoroutine(clearroom());
                GameManager.Instance.winText.text = GameManager.Instance.WinTextChange;
                GameManager.Instance.WinDeskription.text = GameManager.Instance.WinDeskripsiChange;
                GameManager.Instance.ANDText.text = "1";
                GameManager.Instance.hasKeyAND = true;
                Destroy(GameManager.Instance.AND, 2);
                GameManager.Instance.deskripsiGene++ ;

                break;
            case "OR":
                GameManager.Instance.WinTextChange = "GERBANG OR SELESAI";
                GameManager.Instance.WinDeskripsiChange = "SELAMAT!!  kamu telah menyelesaikan perbaikan salah satu ruangan dengan menggunakan gerbang logika OR. Kamu telah memperbaiki generator dan menyalakan salah satu saklar atapun bisa keduanya yang merupakan cara mengalirkan arus menggunakan gerbang logika OR.";
                StartCoroutine(clearroom());
                GameManager.Instance.winText.text = GameManager.Instance.WinTextChange;
                GameManager.Instance.WinDeskription.text = GameManager.Instance.WinDeskripsiChange;
                GameManager.Instance.ORText.text = "1";
                GameManager.Instance.hasKeyOR = true;
                Destroy(GameManager.Instance.OR, 2);
                GameManager.Instance.deskripsiGene++;
                break;
            case "NOT":
                GameManager.Instance.WinDeskripsiChange = "SELAMAT!!  kamu telah menyelesaikan perbaikan salah satu ruangan dengan menggunakan gerbang logika NOT. Kamu telah memperbaiki generator dan apa bila saklar dalam kondisi menyala tetapi lampu tidak menyala maka kamu harus mematikan saklar tersebut karena gerbang NOT merupakan Inverter atau pembalik.";
                GameManager.Instance.WinTextChange = "GERBANG NOT SELESAI";
                StartCoroutine(clearroom());
                GameManager.Instance.winText.text = GameManager.Instance.WinTextChange;
                GameManager.Instance.WinDeskription.text = GameManager.Instance.WinDeskripsiChange;
                GameManager.Instance.NOTText.text = "1";
                GameManager.Instance.hasKeyNOT = true;
                Destroy(GameManager.Instance.NOT, 2);
                GameManager.Instance.deskripsiGene++;
                break;
            case "NAND":
                GameManager.Instance.WinTextChange = "GERBANG NAND SELESAI";
                GameManager.Instance.WinDeskripsiChange = "SELAMAT!!  kamu telah menyelesaikan perbaikan salah satu ruangan dengan menggunakan gerbang logika NAND. Kamu telah memperbaiki generator dan apa bila kedua saklar dalam kondisi menyala tetapi lampu tidak menyala maka kamu harus mematikan kedua saklar tersebut karena gerbang NAND merupakan Inverter atau pembalik dari gerbang AND.";
                StartCoroutine(clearroom());
                GameManager.Instance.winText.text = GameManager.Instance.WinTextChange;
                GameManager.Instance.WinDeskription.text = GameManager.Instance.WinDeskripsiChange;
                GameManager.Instance.NANDText.text = "1";
                GameManager.Instance.hasKeyNAND = true;
                Destroy(GameManager.Instance.NAND, 2);
                GameManager.Instance.deskripsiGene++;
                break;
            case "NOR":
                GameManager.Instance.WinTextChange = "GERBANG NOR SELESAI";
                GameManager.Instance.WinDeskripsiChange = "SELAMAT!!  kamu telah menyelesaikan perbaikan salah satu ruangan dengan menggunakan gerbang logika NOR. Kamu telah memperbaiki generator dan apa bila kedua saklar dalam kondisi menyala tetapi lampu tidak menyala maka kamu harus mematikan salah satu saklar tersebut karena gerbang NAND merupakan Inverter atau pembalik dari gerbang NOR.";
                StartCoroutine(clearroom());
                GameManager.Instance.winText.text = GameManager.Instance.WinTextChange;
                GameManager.Instance.WinDeskription.text = GameManager.Instance.WinDeskripsiChange;
                GameManager.Instance.NORText.text = "1";
                GameManager.Instance.hasKeyNOR = true;
                Destroy(GameManager.Instance.NOR, 2);
                GameManager.Instance.deskripsiGene++;
                break;

            //tower
            case "TWRAND":
                if (GameManager.Instance.towerANDON == false && GameManager.Instance.hasKeyAND == true)
                {
                    GameManager.Instance.deskripsiTower++;
                }
                if (GameManager.Instance.hasKeyAND == true)
                {
                    GameManager.Instance.towerLightAND.SetActive(true);
                    GameManager.Instance.towerSpehereAND.material = GameManager.Instance.SphereTower;
                    GameManager.Instance.towerANDON = true;

                }
                else
                {
                    StartCoroutine(notiftower());
                }
                break;
            case "TWROR":
                if (GameManager.Instance.towerORON == false && GameManager.Instance.hasKeyOR == true)
                {
                    GameManager.Instance.deskripsiTower++;
                }
                if (GameManager.Instance.hasKeyOR == true)
                {
                    GameManager.Instance.towerLightOR.SetActive(true);
                    GameManager.Instance.towerSpehereOR.material = GameManager.Instance.SphereTower;
                    GameManager.Instance.towerORON = true;
                }
                else
                {
                    StartCoroutine(notiftower());
                }
                break;
            case "TWRNOT":
                if (GameManager.Instance.towerNOTON == false && GameManager.Instance.hasKeyNOT == true)
                {
                    GameManager.Instance.deskripsiTower++;
                }
                if (GameManager.Instance.hasKeyNOT == true)
                {
                    GameManager.Instance.towerLightNOT.SetActive(true);
                    GameManager.Instance.towerSpehereNOT.material = GameManager.Instance.SphereTower;
                    GameManager.Instance.towerNOTON = true;
                }
                else
                {
                    StartCoroutine(notiftower());
                }
                break;
            case "TWRNAND":
                if (GameManager.Instance.towerNANDON == false && GameManager.Instance.hasKeyNAND == true)
                {
                    GameManager.Instance.deskripsiTower++;
                }
                if (GameManager.Instance.hasKeyNAND == true)
                {
                    GameManager.Instance.towerLightNAND.SetActive(true);
                    GameManager.Instance.towerSpehereNAND.material = GameManager.Instance.SphereTower;
                    GameManager.Instance.towerNANDON = true;
                }
                else
                {
                    StartCoroutine(notiftower());
                }
                break;
            case "TWRNOR":
                if (GameManager.Instance.towerNORON == false && GameManager.Instance.hasKeyNOR == true)
                {
                    GameManager.Instance.deskripsiTower++;
                }
                if (GameManager.Instance.hasKeyNOR == true)
                {
                    GameManager.Instance.towerLightNOR.SetActive(true);
                    GameManager.Instance.towerSpeherNOR.material = GameManager.Instance.SphereTower;
                    GameManager.Instance.towerNORON = true;

                }
                else
                {
                    StartCoroutine(notiftower());
                }
                break;

           
        }
    }
    IEnumerator notiftower()
    {
        GameManager.Instance.notifText.text = "Kamu belum memiliki kunci";
        GameManager.Instance.notifOBJ.SetActive(true);
        yield return new WaitForSeconds(3);
        GameManager.Instance.notifOBJ.SetActive(false);
    }
    IEnumerator clearroom()
    {
        GameManager.Instance.gameclearPanel.SetActive(true);
        GameManager.Instance.pause = true;
        GameManager.Instance.charaMove = false;
        GameManager.Instance.stopwatchActive = false;
        yield return new WaitForSeconds(10f);
        GameManager.Instance.gameclearPanel.SetActive(false);
        GameManager.Instance.pause = false;
        GameManager.Instance.charaMove = true;
        GameManager.Instance.stopwatchActive = true;
    }
}
