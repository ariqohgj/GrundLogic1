using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoControl : MonoBehaviour
{
    public VideoPlayer vdPlayer;
    public Slider sldVid;
    public VideoClip[] clips;
    public RectTransform image;
    public bool isBig;
    public GameObject PanelIndikator;
    public bool isPlay;
    public Sprite pause;
    public Sprite play;
    public Image pandp;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (vdPlayer.isPlaying)
        {
            sldVid.value = (float)vdPlayer.frame / (float)vdPlayer.frameCount;
        }
    }

    public void changeClip(int numberClip)
    {
        
        switch (numberClip)
        {
            case 1:
                vdPlayer.clip = clips[0];
                break;
            
            case 2:
                vdPlayer.clip = clips[1];
                break;
            case 3:
                vdPlayer.clip = clips[2];
                break;
            case 4:
                vdPlayer.clip = clips[3];
                break;
            case 5:
                vdPlayer.clip = clips[4];
                break;
            case 6:
                vdPlayer.clip = clips[5];
                break;

        }
    }
    public void changeSize()

    {
        if (!isBig)
        {
            image.sizeDelta = new Vector2(1920, 1080);
            image.localPosition = new Vector2(0 , image.localPosition.y);
            isBig = true;
        
        }else if (isBig)
        {
            image.sizeDelta = new Vector2(1280, 720);
            image.localPosition = new Vector2(-270, image.localPosition.y);
            isBig = false;
        }

    }
    public void hideIndikator()
    {
        StartCoroutine(hideIndicators());
    }
    public void pauseAndPlay()
    {
        
        if (!isPlay)
        {
            vdPlayer.Play();
            isPlay = true;
            pandp.sprite = pause;

        }
        else if (isPlay)
        {
            vdPlayer.Pause();
            isPlay = false;
            pandp.sprite = play;
        }
    }

    IEnumerator hideIndicators()
    {
        PanelIndikator.SetActive(true);
        yield return new WaitForSeconds(3);
        PanelIndikator.SetActive(false);
    }
}
