using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeSceneStart : MonoBehaviour
{
    public string scenename;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(changeScene());
    }

    // Update is called once per frame
    void Update()
    {
        scenename = PlayerPrefs.GetString("NextSc");
    }
    IEnumerator changeScene()
    {
        yield return new WaitForSeconds(30);
        SceneManager.LoadSceneAsync(scenename);
    }
}
