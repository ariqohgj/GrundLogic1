using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuControl : MonoBehaviour
{
    public GameObject panelanim;

    public void NextScene(string sceneName)
    {
        StartCoroutine(changescene(sceneName));
    }
    public void NextSceneNoAnim(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }
    public void Exit()
    {
        Application.Quit();
    }
    public void setScene()
    {
        PlayerPrefs.SetString("NextSc", "InputNama");
    }
    public void setScene1()
    {
        PlayerPrefs.SetString("NextSc", "MAP_1_REV");
    }
    IEnumerator changescene(string scenename)
    {
        panelanim.SetActive(true);
        yield return new WaitForSeconds(1.3f);
        SceneManager.LoadSceneAsync(scenename);

    }
}
