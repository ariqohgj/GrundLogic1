using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TutorialScript : MonoBehaviour
{
    public string kontrol;
    public string Gameplay1;
    public string Gameplay2;
    public string Gameplay3;
    public string Gameplay4;
    public string Petunjuk1;
    public string Petunjuk2;

    public string kontrol_title;
    public string Gameplay1_title;
    public string Gameplay2_title;
    public string Gameplay3_title;
    public string Gameplay4_title;
    public string Petunjuk1_title;
    public string Petunjuk2_title;

    public Image tutorialImage;
    public GameObject PanelTutor;
    public GameObject btnTutup;
    public TextMeshProUGUI textTitle;
    public TextMeshProUGUI textDeskripsi;

    public Sprite kontrol_image;
    public Sprite Gameplay1_image;
    public Sprite Gameplay2_image;
    public Sprite Gameplay3_image;
    public Sprite Gameplay4_image;
    public Sprite Petunjuk1_image;
    public Sprite Petunjuk2_image;

    public int index;
    // Start is called before the first frame update
    void Start()
    {
        index = 1;
    }


    // Update is called once per frame
    void Update()
    {
        if (index > 7)
        {
            index = 1;
        }else if(index < 1)
        {
            index = 1;
        }

        switch (index)
        {
            case 1:
                tutorialImage.sprite = kontrol_image;
                textTitle.text = kontrol_title;
                textDeskripsi.text = kontrol;
                btnTutup.SetActive(false);
                break;
            case 2:
                tutorialImage.sprite = Gameplay1_image;
                textTitle.text = Gameplay1_title;
                textDeskripsi.text = Gameplay1;
                break;
            case 3:
                tutorialImage.sprite = Gameplay2_image;
                textTitle.text = Gameplay2_title;
                textDeskripsi.text = Gameplay2;
                break;
            case 4:
                tutorialImage.sprite = Gameplay3_image;
                textTitle.text = Gameplay3_title;
                textDeskripsi.text = Gameplay3;
                break;
            case 5:
                tutorialImage.sprite = Gameplay4_image;
                textTitle.text = Gameplay4_title;
                textDeskripsi.text = Gameplay4;
                break;
            case 6:
                tutorialImage.sprite = Petunjuk1_image;
                textTitle.text = Petunjuk1_title;
                textDeskripsi.text = Petunjuk1;
                break;
            case 7:
                tutorialImage.sprite = Petunjuk2_image;
                textTitle.text = Petunjuk2_title;
                textDeskripsi.text = Petunjuk2;
                btnTutup.SetActive(true);
                break;
        }
    }
    
    public void indexPlus()
    {
        index++;
    }
    public void indexMinus()
    {
        index--;
    }
    public void tutupPanel()
    {
        PanelTutor.SetActive(false);
        GameManager.Instance.pause = false;
        GameManager.Instance.stopwatchActive = true;
        // Lock cursor
        GameManager.Instance.walkingSpeed = GameManager.Instance.walkingSpeedDefault;
        GameManager.Instance.runningSpeed = GameManager.Instance.runningSpeedDefault;
        GameManager.Instance.lookSpeed = GameManager.Instance.lookSpeedDefault;
        GameManager.Instance.jumpSpeed = GameManager.Instance.jumpSpeedDefault;

    }

}
