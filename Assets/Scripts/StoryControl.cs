using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class StoryControl : MonoBehaviour
{
    public GameObject hideUnhideUI;
    
    public GameObject[] dialog;
    public bool NotMove;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(NotMove == true )
        {
            GameManager.Instance.charaMove = false;
            hideUnhideUI.SetActive(false);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }
    void Awake()
    {

    }

    public void CloseDialog()
    {
        StartCoroutine(CloseDialoginTime());
    }
    public void NextDialog(int dialognumber)
    {
        switch (dialognumber)
        {
            case 1:
                dialog[0].SetActive(false);
                dialog[1].SetActive(true);
                break;
            case 2:
                dialog[1].SetActive(false);
                dialog[2].SetActive(true);
                break;
            case 3:
                dialog[2].SetActive(false);
                dialog[3].SetActive(true);
                break;
            case 4:
                dialog[3].SetActive(false);
                dialog[4].SetActive(true);
                break;
            case 5:
                dialog[4].SetActive(false);
                dialog[5].SetActive(true);
                break;
            case 6:
                dialog[5].SetActive(false);
                dialog[6].SetActive(true);
                break;
            case 7:
                dialog[6].SetActive(false);
                dialog[7].SetActive(true);
                break;
            case 8:
                dialog[7].SetActive(false);
                dialog[8].SetActive(true);
                break;
            case 9:
                dialog[8].SetActive(false);
                GameManager.Instance.gamestartPanel.SetActive(true);
                break;
            case 10:
                dialog[9].SetActive(false);
                dialog[10].SetActive(true);
                break;
            case 11:
                dialog[10].SetActive(false);
                break;


        }
    }
    IEnumerator CloseDialoginTime()
    {
        NotMove = false;
        GameManager.Instance.charaMove = true;
        yield return new WaitForSeconds(0);
        GameManager.Instance.panelStory.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        hideUnhideUI.SetActive(true);
        GameManager.Instance.pause = false;
    }
    public void openfirstDialog()
    {
        hideUnhideUI.SetActive(false);
        NotMove = true;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        GameManager.Instance.pause = true;
    }
}
