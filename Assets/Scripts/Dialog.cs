using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Dialog : MonoBehaviour
{
    public GameObject story1;
    public GameObject story2;
    public GameObject story3;
    public GameObject story4;
    public GameObject story5;
    public GameObject PanelBlock;
    public GameObject PanelRasi;
    public GameObject sirkel;
    public GameObject click;
    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("gas");
        speed = 0;
    }

    // Update is called once per frame
    void Update()
    {
        PanelRasi.gameObject.transform.localScale = Vector3.Lerp(PanelRasi.gameObject.transform.localScale, new Vector3(5, 5, 5), speed * Time.deltaTime);
    }
    IEnumerator gas()
    {
        yield return new WaitForSeconds(7);
        story1.SetActive(false);
        story2.SetActive(true);
        yield return new WaitForSeconds(9);
        story2.SetActive(false);
        yield return new WaitForSeconds(1);
        story3.SetActive(true);
        yield return new WaitForSeconds(3);
        PanelBlock.SetActive(true);
        yield return new WaitForSeconds(3);
        story3.SetActive(false);
        story4.SetActive(true);
        sirkel.GetComponent<Image>().color = Color.grey;
        yield return new WaitForSeconds(7);
        story4.SetActive(false);
        PanelBlock.SetActive(false);
        yield return new WaitForSeconds(2);
        story5.SetActive(true);
        speed = 1;
        yield return new WaitForSeconds(7);
        story5.SetActive(false);
        yield return new WaitForSeconds(2);
        click.SetActive(true);
    }
}
